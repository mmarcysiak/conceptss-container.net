﻿using System;

namespace PK.Container
{
    public class UnresolvedDependenciesException : InvalidOperationException
    {
        public UnresolvedDependenciesException() : base() { }

        public UnresolvedDependenciesException(string message) : base(message) { }

        public UnresolvedDependenciesException(string message, Exception innerException) : base(message, innerException) { }
    }
}
